import 'package:flutter/material.dart';
import 'package:star_war_app/people_list.dart';
import 'package:star_war_app/providers/db_provider.dart';
import 'package:star_war_app/providers/db_provider.dart';

class EditPeople extends StatefulWidget {
  final int id;
  final String name;

  const EditPeople({Key key, this.id, this.name}): super(key: key);
  _EditPeopleState createState () => _EditPeopleState(name: this.name, id: this.id);
}

class _EditPeopleState extends State<EditPeople> {
  int id;
  String name;

  _EditPeopleState({ this.id, this.name});

  final nameController = TextEditingController(text: '');

  @override
  void initState() {
    super.initState();
    nameController.text = name;
  }

  _createPeople() async {
    DBProvider.db.updatePeopleById(id, nameController.text);

    await Future.delayed(const Duration(seconds: 2));
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => PeopleList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final nameInpuField = Container(
      // color: Colors.white,
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
      margin: EdgeInsets.only(bottom: 10.0),
      decoration: new BoxDecoration(
        color: Colors.white.withOpacity(0.7),
        border: Border.all(color: Colors.grey.withOpacity(0.5)),
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
      child: new TextField(
        controller: nameController,
        style: new TextStyle(color: Colors.grey),
        decoration: new InputDecoration(
          hintText: 'Name',
          hintStyle: TextStyle(color: Colors.grey),
          border: InputBorder.none,
          contentPadding: EdgeInsets.all(15.0),),
      ),
    );

    final saveButton = InkWell(
      onTap: () {
        _createPeople();
      },
      child: Container(
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
            color: Colors.blueAccent,
            borderRadius: BorderRadius.all(Radius.circular(3))
        ),
        child: Text('Save', style: TextStyle(color: Colors.white),),
      ),
    );

    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text('Edit People'),
        ),
        body: SafeArea(
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 10,
                  child: Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: <Widget>[
                        nameInpuField,
                        saveButton
                      ],
                    ),
                  ),
                )
              ],
            )
        ),
      ),
    );
  }
}
