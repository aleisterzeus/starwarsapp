import 'package:flutter/material.dart';
import 'package:star_war_app/people_list.dart';
import 'package:star_war_app/create_people.dart';
import 'package:star_war_app/edit_people.dart';

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: PeopleList(),
      routes: <String, WidgetBuilder> {
        'people-list': (context) => PeopleList(),
        'create-people': (context) => CreatePeople(),
        'edit-people': (context) => EditPeople()
      },
    );
  }
}

void main() => runApp(MyApp());
