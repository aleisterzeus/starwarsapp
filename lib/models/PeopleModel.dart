import 'dart:convert';

//List<PeopleModel> employeeFromJson(String str) =>
//    List<PeopleModel>.from(json.decode(str).map((x) => PeopleModel.fromJson(x)));
//
//String employeeToJson(List<PeopleModel> data) =>
//    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PeopleModel {
  int id;
  String name;
  String height;

  PeopleModel({
    this.id,
    this.name,
    this.height
  });

  factory PeopleModel.fromJson(Map<String, dynamic> json) => PeopleModel(
    id: json["id"],
    name: json["name"],
    height: json["height"]
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "height": height
  };
}
