import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:path/path.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:star_war_app/edit_people.dart';
import 'package:star_war_app/models/PeopleModel.dart';
import 'package:star_war_app/providers/api_provider.dart';
import 'package:star_war_app/providers/db_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PeopleList extends StatefulWidget {
  _PeopleListState createState() => _PeopleListState();
}

class  _PeopleListState extends State<PeopleList> {
  var isLoading = false;
  var orderBy = 'DESC';
  Future<List<PeopleModel>> peoples;

  @override
  void initState() {
    super.initState();
    _checkingData();
  }

  _checkingData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool('initialized') == null) {
      await _initializedDataFromApi();
    }
    setState(() {
      peoples = DBProvider.db.getAllPeoples('DESC');
    });
  }

  _initializedDataFromApi() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      isLoading = true;
    });

    var apiProvider = ApiProvider();
    await apiProvider.getAllPeoples();
    prefs.setBool('initialized', true);
    await Future.delayed(const Duration(seconds: 2));

    setState(() {
      isLoading = false;
    });
  }

  _deletePeopleById(int id) async {
    await DBProvider.db.deletePeopleByid(id);
    await Future.delayed(const Duration(seconds: 2));
    setState(() {
      peoples = DBProvider.db.getAllPeoples('DESC');
    });
  }

  showAlertDialog(BuildContext context, int id) {

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed:  () {Navigator.of(context, rootNavigator: true).pop('dialog');},
    );
    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed:  () {
        _deletePeopleById(id);
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Confirmation"),
      content: Text("Are you sure want to delete this people ?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _renderPeopleList(BuildContext ctx) {
    return FutureBuilder(
      future: peoples,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator(backgroundColor: Colors.red,),);
        }
        return ListView.separated(
          separatorBuilder: (context, index) => Divider(
            color: Colors.black12,
          ),
          itemCount: snapshot.data.length,
          itemBuilder: (BuildContext context, int index) {
            var data = snapshot.data[index];
            return Container(
              alignment: Alignment.topLeft,
              margin: EdgeInsets.only(bottom: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5))
              ),
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(data.name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                        Text('${data.height} cm', style: TextStyle(color: Colors.grey, fontSize: 14),)
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(left: 10),
                    child: Row(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                           showAlertDialog(context, data.id);
                          },
                          child: Container(
                            margin: EdgeInsets.only(right: 10),
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Colors.redAccent,
                                borderRadius: BorderRadius.all(Radius.circular(5))
                            ),
                            child: Icon(Icons.delete_outline, color: Colors.white,),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => EditPeople(name: data.name,id: data.id,),
                              ),
                            );
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Colors.blueAccent,
                                borderRadius: BorderRadius.all(Radius.circular(5))
                            ),
                            child: Icon(Icons.edit, color: Colors.white,),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('People List'),
        ),
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.all(10),
            color: Colors.grey.withOpacity(0.3),
            child: isLoading ? Center(child: CircularProgressIndicator(),):
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    if (orderBy == 'DESC') {
                      setState(() {
                        orderBy = 'ASC';
                        peoples = DBProvider.db.getAllPeoples('ASC');
                      });

                    } else {
                      setState(() {
                        orderBy = 'DESC';
                        peoples = DBProvider.db.getAllPeoples('DESC');
                      });
                    }
                  },
                  child: Container(
                    alignment: Alignment.topLeft,
                    decoration: BoxDecoration(
                      color: Colors.blueAccent,
                      borderRadius: BorderRadius.all(Radius.circular(5))
                    ),
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                       Padding(
                         padding: EdgeInsets.only(right: 10),
                         child:  Text(orderBy, style: TextStyle(color: Colors.white),),
                       ),
                        Icon(orderBy == 'DESC' ? Icons.arrow_downward:Icons.arrow_upward, color: Colors.white,)
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 10,
                  child: _renderPeopleList(context),
                )
              ],
            )
            ,
          )
        ),
        floatingActionButton: new FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, 'create-people');
          },
          child: const Icon(Icons.add),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        ),
      );
  }
}
