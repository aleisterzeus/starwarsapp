import 'package:star_war_app/models/PeopleModel.dart';
import 'package:star_war_app/providers/db_provider.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ApiProvider {
  Future<List<PeopleModel>> getAllPeoples() async {
    final JsonDecoder json = new JsonDecoder();
    http.Response response = await http.get(
        'https://swapi.co/api/people/');
    var responseJson = json.convert(response.body);
    return (responseJson['results'] as List).map((people) {
      print('Insertingg $people');
      DBProvider.db.initializedData(PeopleModel.fromJson(people));
    }).toList();
  }
}
