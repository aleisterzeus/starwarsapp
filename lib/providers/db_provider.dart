import 'dart:io';
import 'package:star_war_app/models/PeopleModel.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database> get database async {
//    await sqflite.deleteDatabase('peoples.db');
    // If providers exists, return providers
    if (_database != null) return _database;
    // If providers don't exists, create one
    _database = await initDB();

    return _database;
  }

  // Create the providers and the {People} table

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'peoples.db');

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
          await db.execute('CREATE TABLE People('
              'id INTEGER PRIMARY KEY,'
              'name TEXT,'
              'height TEXT'
              ')');
        });
  }

  // Insert People on providers
  initializedData(PeopleModel newPeople) async {
    await deleteAllPeoples();
    final db = await database;
    final res = await db.insert('People', newPeople.toJson());

    return res;
  }

  //create people by input user
  createPeople(String name) async {
    final db = await database;
    final res = await db.rawInsert('INSERT INTO People(name) VALUES("$name")');

    return res;
  }

  // Delete all People
  Future<int> deleteAllPeoples() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM People');

    return res;
  }

  // Delete people by id
  Future<int> deletePeopleByid(int id) async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM People where id = $id');

    return res;
  }

  Future<int> updatePeopleById(int id, String name) async {
    final db = await database;
    final res = await db.rawUpdate('UPDATE People SET name = "$name" where id = $id');

    return res;
  }

  Future<List<PeopleModel>> getAllPeoples(String orderBy) async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM People ORDER BY id $orderBy");

    List<PeopleModel> list =
    res.isNotEmpty ? res.map((c) => PeopleModel.fromJson(c)).toList() : [];

    return list;
  }
}
